# uml

UML-Diagrams is by the dcdg dart plugin. I use the darry dart script manager to make my pub calls which simplifies the basic flutter project automation one has to create.

The article how to set up using dcdg is, [@medium](), and the article on how to use the derry dart plugin is, [@medium]().

## Getting Started

This project is a starting point for a Flutter application.

Some resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
