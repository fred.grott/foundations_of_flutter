

// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license

import 'package:flutter/widgets.dart';

class AppConfig extends InheritedWidget {
  const AppConfig({

    @required this.appName,
    @required this.flavorName,
    @required this.apiBaseUrl,

    @required Widget child,
  }) : super(child: child);


  final String appName;
  final String flavorName;
  final String apiBaseUrl;

  static AppConfig of(BuildContext context) {
    // .inheritFromWidgetOfExactType is depreciated past 1.12.1
    return context.dependOnInheritedWidgetOfExactType(aspect: AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
