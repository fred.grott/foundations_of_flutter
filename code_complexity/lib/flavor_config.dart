// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license

import 'package:flutter/foundation.dart';

/// Returns the correct build mode as string flavor
/// @author Fredrick Allan Grott
String get flavor {
  String flavorMode;
  // constants, see foundations constants.dart
  if(kDebugMode){
    flavorMode='debug';
  }
  if(kReleaseMode){
    flavorMode ='release';
  }
  return flavorMode;
}