

// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license

import 'package:flutter/widgets.dart';

/// Implements how to config Widget Debug settings. Get's called as
/// ```dart
/// final WidgetDebugConfig widgetDebugConfig = WidgetDebugConfig(
///   appDebugPrintRebuildDirtyWidgets:  true,
///   );
/// ```
/// @author Fredrick Allan Grott
class WidgetDebugConfig {
  const WidgetDebugConfig({
    @required this.appDebugPrintRebuildDirtyWidgets,
    @required this.appDebugPrintBuildScope,
    @required this.appDebugPrintScheduleBuildForStacks,
    @required this.appDebugPrintGlobalKeyedWidgetLifecycle,
    @required this.appDebugProfileBuildsEnabled,
    @required this.appDebugHighlightDeprecatedWidgets
  });

  final bool appDebugPrintRebuildDirtyWidgets;
  final bool appDebugPrintBuildScope;
  final bool appDebugPrintScheduleBuildForStacks;
  final bool appDebugPrintGlobalKeyedWidgetLifecycle;
  final bool appDebugProfileBuildsEnabled;
  final bool appDebugHighlightDeprecatedWidgets;
}