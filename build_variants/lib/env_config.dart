// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license

// class EnvironmentConfig {
//   static const APP_NAME = String.fromEnvironment(
//     'DEFINEEXAMPLE_APP_NAME',
//     defaultValue: 'awesomeApp'
//   );
//   static const APP_SUFFIX = String.fromEnvironment(
//       'DEFINEEXAMPLE_APP_SUFFIX'
//   );
// }

class EnvironmentConfig {
  static const String AppName = String.fromEnvironment(
      'APP_NAME', defaultValue: 'build variants');
// add other compile vars to impl here
}