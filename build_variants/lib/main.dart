import 'package:build_variants/widget_debug_config.dart';
import 'package:flutter/material.dart';

import 'app_config.dart';
import 'env_config.dart';
import 'my_app.dart';

//note: this is the actual prod variant and debug variant is run by config
//      either IDE run or stating the file url at end of the flutter run command
void main() {
  const WidgetDebugConfig widgetDebugConfig = WidgetDebugConfig(
    appDebugPrintRebuildDirtyWidgets: false,
    appDebugHighlightDeprecatedWidgets: false,
    appDebugProfileBuildsEnabled: false,
    appDebugPrintBuildScope: false,
    appDebugPrintGlobalKeyedWidgetLifecycle: false,
    appDebugPrintScheduleBuildForStacks: false,
  );
  final AppConfig configuredApp = AppConfig(
    appName: EnvironmentConfig.AppName,
    flavorName: 'production',
    apiBaseUrl: 'https://api.example.com/',
    child: MyApp(),
  );

  runApp(configuredApp);
}