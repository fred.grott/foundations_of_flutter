// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license

import 'package:build_variants/widget_debug_config.dart';
import 'package:flutter/widgets.dart';

import 'app_config.dart';
import 'env_config.dart';
import 'flavor_config.dart';
import 'my_app.dart';

void main() {


  const WidgetDebugConfig widgetDebugConfig = WidgetDebugConfig(
    appDebugPrintRebuildDirtyWidgets: false,
    appDebugHighlightDeprecatedWidgets: false,
    appDebugProfileBuildsEnabled: false,
    appDebugPrintBuildScope: false,
    appDebugPrintGlobalKeyedWidgetLifecycle: false,
    appDebugPrintScheduleBuildForStacks: false,
  );
  final AppConfig configuredApp = AppConfig(
    appName: EnvironmentConfig.AppName,
    flavorName: flavor,
    apiBaseUrl: 'https://dev-api.example.com/',
    child: MyApp(),
  );

  runApp(configuredApp);
}