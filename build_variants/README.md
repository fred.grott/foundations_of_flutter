# build_variants

This project uses the dcdg uml generator plugin and the dart derry script manager plugins.

## Resources
[Build Variants@medium]()
[UML Generation@medium]()
[Derry Dart Script Manager@medium]()

## Getting Started

This project is a starting point for a Flutter application.

Some resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
