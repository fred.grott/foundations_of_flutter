![lint](/images/lint.png)
# lint

Like the Flutter and Dart contributors I use the have the full lint rules used in the project which means I copy and paste the subset of lint rules used into this project.  It reduces the wokring memory load of having to remember what is used in a lint plugin.

The plugin used is PascalWelsch's

[Lint@pub.dev](https://pub.dev/packages/lint)
[Lint@github](https://github.com/passsy/dart-lint)

It's a more opnionated approach to applying lint rules and work's well with migrating to
the future dart language improvements with less fuss and mess.

## Screenshots

![lint screenshot](/images/lint-screenshot-640-369.png)
![lint hints](/images/lint-hints-screenshot-1280-245.png)

## Article

A in-depth article is at:

[Article@medium.com]()

## Getting Started

This project is a starting point for a Flutter application.

Some resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
