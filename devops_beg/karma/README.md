<!-- PROJECT LOGO  should always be somewhat the twitter card size-->
![gitlab repo card](./images/karma-project.png)


# Karma
<!-- Description should be a story in sentence fragment form describing the project's value proposition to the developer, designer, etc.The general story form is of a short joke. Namely the struggle, the discovery, and the surprise. I found I could not do AAA until I shot Buddha and this Project helps you Shoot Buddha.-->

A template for the correct Flutter project set-up including Domain Driven Design  app architecture onion layering, correct widget test set-up, the correct instrumented testing set-up, a flexible workflow automation integration, etc.

All reachable from the beginning flutter developer's point of view.

[**Explore the docs**](https://gitlab.com/fred.grott/foundations_of_flutter/devops_beg/karma), it's part of the full [foundations_of_flutter](https://gitlab.com/fred.grott/foundations_of_flutter) project which is a code repo that I am creating to cover the flutter articles I write and publish on the medium writing platform.


[View Demo](https://gitlab.com/fred.grott/foundations_of_flutter/devops-beg/karma)


[Report A bug](https://gitlab.com/fred.grott/foundations_of_flutter/-/issues)



[Request a feature](https://gitlab.com/fred.grott/foundations_of_flutter/-/issues)


<!-- PROJECT SHIELDS gitlab has not changed how they integrate with shields.io yet doing to auth tokens but we still can
     read the information  we need without an auth token as of Jan 2021
     replace `https://gitlab.com/api/v4/projects/23873593` and use onw gitlab project ID .-->


<!-- TABLE OF CONTENTS gitlab does auto via their GFM one needs to add a toc tag onits own line-->


[[_TOC_]]

<!-- ABOUT THE PROJECT -->
## About The Project

<!-- Always wrap to center as you may have more than one screenshot to show.The length of table-of-contents determines whether the screenshot gets placed here or after the project shields as we want at least parth of the screenshots above the fold in the desktop display of the webpage generated from the markdown.-->

I believe that you, the beginning flutter developer, should be starting with the right project set-up and the right feedback tools from the beginning of the your learning flutter adventure. Copy and paste and modfiy this to fit your blnk project so that you can start right from the very beginning.

### Built With


* [Android Studio IDE](https://developer.android.com/studio/)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

Most front-end application code requires usage with the git tool. To install git:

* [How To Install Git](https://git-scm.com/)

For Flutter Mobile Applications you need to install at least one mobile SDK:

* [How To Install Android SDK](https://anddroid.dev)
* [How To Install iOS SDK on MACOSX install XCode to get it](https://apps.apple.com/us/app/xcode/id497799835?mt=12)

Than install the Flutter SDK

* [How To Insall  the Flutter SDK](https://flutter.dev/docs/get-started/install.html)

Than install an IDE

* [How To install the Android IDE](https://developer.android.com/studio)
* [How To install the MS VSCode IDE](https://code.visualstudio.com/)


### Installation
1.  Clone the repo via _sh_

        git clone https://gitlab.com/fred.grott/foundations_of_flutter.git


2.  Open the karma sub-project in your Android Studio or MS VSCode IDE, at foundations_of_flutter/devops_beg/karma


<!-- USAGE EXAMPLES -->
## Usage

You will copy and paste files from lib, test, test_drive and integration_test to your project folder and change things like your app name. You will copy the derry.yaml, dartdoc_options, and  analysis_options to your project.

Set this line in your gitignore:
```
**/failures/*.png
```

Create a separate image folder as that is required by the goldens_toolkit library I am using from eBay Inc for more goldens testing power.

Last, but not least you need this stuff in your pubspec, under dependencies
```
meta: ^1.2.4
golden_toolkit: ^0.8.0

```
under dev_dependencies:
```
integration_test: ^1.0.2+2
build_runner: ^1.11.0
dart_code_metrics: ^2.4.0

```

And, on the last line of your pbuspec put this:
```
scripts: derry.yaml

```
That is required for the derry dart plugin that provides the workflow automation. Also required for the workflow automation is to have perl and python installed as I use two scripts to transform xml reports to html that have those dependencies.


<!-- ROADMAP -->
## Roadmap





<!-- CONTRIBUTING -->
## Contributing



<!-- CONTRIBUTORS-->
### Contributors




<!-- LICENSE -->
## License

Distributed under the BSD License. See [LICENSE](https://gitlab.com/fred.grott/foundations_of_flutter/-/blob/master/LICENSE) for more information.



<!-- CONTACT -->
## Contact
<!-- email addy is always stated in an expanded way as it obfuscates it from picked up by web scrapers and generating spam emails-->
Fred Grott - [@twitter_handle](https://twitter.com/fredgrott) - email: fred DOT grott AT gmail DOT com





<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements



<!-- RESOURCES-->
<!-- You should put some resources to the computer language, frameworks, etc as it decreases someone opening issues of how to use that computer laguage or framework.-->
## Flutter Community Resources
* [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
* [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
* [Flutter online documentation](https://flutter.dev/docs)
* [Dart online documentation](https://dart.dev/docs)
* [Ask Flutter Dev questions@Stackoverflow](https://stackoverflow.com/tags/flutter)
* [Ask Flutter Dev questions@Reddit](https://www.reddit.com/r/FlutterDev/)
* [Flutter Community Articles@Medium.com](https://medium.com/flutter-io)
* [Solido Awesome Flutter List Of Resources](https://github.com/Solido/awesome-flutter)
* [Flutter Dev Videos@Youtube](https://www.youtube.com/playlist?list=PLOU2XLYxmsIJ7dsVN4iRuA7BT8XHzGtCr)
* [Ask questions about Flutter@Hashnode](https://hashnode.com/n/flutter)
